# Simulador Path Planning: Grafos de visibilidad

_Programa simulador interactivo de navegación de robots, particularmente de la plataforma robótica ARMOS TurtleBot 1 del grupo de investigación ARMOS, para diferentes ambientes de navegación bajo el esquema Grafos de Visibilidad. El programa permite la configuración a necesidad del usuario del robot, de la topología del ambiente de navegación, puntos inicial y final del robot, y parámetros específicos del algoritmo de navegación. El programa genera representaciones gráficas de las posibles rutas y selecciona la óptima para el robot. Los parámetros del esquema pueden modificarse interactivamente para observar el comportamiento de la estrategia. Proyecto desarrollado por el grupo de investigación ARMOS._

## Autores

* **Yeisson F. Triviño R.**
* **Jorge A. Duarte A.**
* **Fredy H. Martínez S.**


### Pre-requisitos

* Python 3.8.5
* tkinter 8.6
* numpy 1.17.4
* OpenCV 4.4.0
* pickle 4.0
* PIL 7.0.0
* bresenham 0.2.1
* ast 0.0.2


## Expresiones de Gratitud 🎁

* Muchas gracias a los miembros del grupo ARMOS por el apoyo en la definición de necesidades y el desarrollo de las pruebas de desempeño.
