import tkinter
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from numpy import *
import numpy as np
import cv2
import math
from bresenham import bresenham
import pickle
from PIL import ImageGrab, Image, ImageTk
from os import remove

from ast import literal_eval
from tkinter import messagebox
import sys
Medidor = 0
NumeroLados = []
Objetos = []
CantidadObjetos = []

python_green = "#476042"
#"------COLORES-------------------------
color1 = "gray22"
color2 = "gray32"
color3 = "gray39"
letra = "snow"
#--------------------------------------------------

ventana = Tk()
ventana.title('Simulador Path Planning: Grafos de visibilidad')
#ventana.iconbitmap("icons\icon.ico")
ventana.iconphoto(True, PhotoImage(file="icon.png"))
ventana.geometry("400x400")#768 "x"
ventana.config(bg= color1)
ventana.maxsize(1360, 700)


#.............................................................................................



#second_frame =Frame(canvas)

#canvas.create_window((0,0),window=second_frame, anchor="nw")
#canvas.config(bd=0) ?????????????????????????????

my_notebook = ttk.Notebook(ventana)
my_notebook.pack(pady=15,anchor="nw",fill="both")


my_frame1 = Frame(my_notebook, width=1360, height=80, bg=color3)
my_frame2 = Frame(my_notebook, width=1360, height=80, bg=color3)
my_frame3 = Frame(my_notebook, width=1360, height=80, bg=color3)
my_frame4 = Frame(my_notebook, width=1360, height=80, bg=color3)

my_frame1.pack(fill="both", expand=1)
my_frame2.pack(fill="both", expand=1)
my_frame3.pack(fill="both", expand=1)
my_frame4.pack(fill="both", expand=1)


my_notebook.add(my_frame1, text="Inicio")
my_notebook.add(my_frame2, text="Analizar")
my_notebook.add(my_frame3, text="Configurar")
my_notebook.add(my_frame4, text="Ayuda")


main_frame =Frame(ventana) #bg=color3)
main_frame.pack(fill=BOTH, expand=1)

#Create a canvas
canvas = Canvas(main_frame)#550
canvas.pack(side=LEFT, fill=BOTH, expand=1)

def _on_mouse_wheel(event):   #permite utilizar el Scroll
    canvas.yview_scroll(-1 * int((event.delta / 120)), "units")


canvas.bind_all("<MouseWheel>", _on_mouse_wheel)

my_scrollbar =ttk.Scrollbar(main_frame, orient= VERTICAL, command =canvas.yview)
my_scrollbar.pack(side=RIGHT, fill=Y)

canvas.configure(yscrollcommand=my_scrollbar.set,bg=color1)
canvas.bind('<Configure>', lambda e:canvas.configure(scrollregion= canvas.bbox("all")))




#-------------------------------------------------DEFINICIONES-------------------------------------------------------
def cajaEscala():

    escalaImagen = 1/100
    Ancho = 25
    Largo = 10
    sensibilidad = 0.0009
    error = -8

    def add():
        try:
            global Ancho, Largo, escalaImagen
            opciones = ["cm", "m", "dam", "hm", "km"]
            unidades = [1, 1 / 100, 1 / 1000, 1 / 10000, 1 / 100000]
            escalaImagen = int(entrada1.get()) * unidades[opciones.index(lista_desplegable.get())]
            Ancho = int(anchoRobot.get())
            Largo = int(largoRobot.get())
            sensibilidad = horizontal.get()
            error = horizontal2.get()




            fichero_Escala8 = open("Escala", "wb")
            pickle.dump(escalaImagen, fichero_Escala8)
            pickle.dump(Ancho, fichero_Escala8)
            pickle.dump(Largo, fichero_Escala8)
            pickle.dump(sensibilidad, fichero_Escala8)
            pickle.dump(error, fichero_Escala8)
            fichero_Escala8.close()

            global Boton_Borde, Boton_Inicial, Boton_Final, Boton_Rutas, Boton_Dijkstra
            Boton_Borde.config(state=NORMAL)
            Boton_Inicial.config(state=DISABLED)
            Boton_Final.config(state=DISABLED)
            Boton_Rutas.config(state=DISABLED)
            Boton_Dijkstra.config(state=DISABLED)

        except:
            pass

    caja = Toplevel()
    caja.geometry("380x550+450+100")
    caja.title("Configuraciones")
    caja.configure(bg=color2)

    textoscaja = Label(caja, text= " Ingrese aquí la escala de la imagen",bg=color1,fg="white")
    textoscaja.pack(padx=5,pady=5,ipadx=5,ipady=5, fill=X)

    entrada1=Entry(caja, justify=CENTER)
    entrada1.insert(END, '1')
    entrada1.pack( padx=5, pady=5, ipadx=5, ipady=5)


    textostop = Label(caja, text=" Ingrese aquí, el ancho del robot (cm)", bg=color1, fg="white")
    textostop.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    anchoRobot = Entry(caja, justify=CENTER)
    anchoRobot.insert(END, '25')
    anchoRobot.pack(padx=5, pady=5, ipadx=5, ipady=5)

    texto2top = Label(caja, text=" Ingrese aquí, la velocidad del robot (cm/s)", bg=color1, fg="white")
    texto2top.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    largoRobot = Entry(caja, justify=CENTER)
    largoRobot.insert(END, '10')
    largoRobot.pack(padx=5, pady=5, ipadx=5, ipady=5)

    texto3top = Label(caja, text="Sensibilidad, imagenes con detalles: 0,0009, ordinarias: 0,005 ", bg=color1, fg="white")
    texto3top.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    var = DoubleVar()
    horizontal= Scale(caja, from_=0.0009, to=0.005, orient=HORIZONTAL, bg=color1,fg="white", variable=var,resolution =0.0001, digits=6)
    horizontal.set(0.0009)
    horizontal.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    texto4top = Label(caja, text=" Ingrese aquí, el error en pixeles", bg=color1, fg="white")
    texto4top.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    horizontal2 = Scale(caja, from_=-3, to=-100, orient=HORIZONTAL, bg=color1)
    horizontal.set(-8)
    horizontal2.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    botonGuardar = Button(caja, text='Guardar valor', command=add)
    botonGuardar.pack(side=TOP,padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    boton2 = Button(caja, text='Salir', command=caja.destroy)
    boton2.pack(side=TOP,padx=5, pady=5, ipadx=5, ipady=5, fill=X)


    lista_desplegable = ttk.Combobox(caja,width=5)
    lista_desplegable.place(x=260,y=50)
    opciones=["cm","m","dam","hm","km"]
    lista_desplegable['value']=opciones
    lista_desplegable.set('m')


    fichero_Escala = open("Escala", "wb")
    pickle.dump(escalaImagen, fichero_Escala)
    pickle.dump(Ancho, fichero_Escala)
    pickle.dump(Largo, fichero_Escala)
    pickle.dump(sensibilidad, fichero_Escala)
    pickle.dump(error, fichero_Escala)
    fichero_Escala.close()

#Button(caja, text="Add", command=add).pack()

class Salida:
    def __init__(self):
        ruta = 0
        NodosObstaculos = 0
        Matrizimg = 0
        Nodos = 0
        MatrizPesoCorregida = 0
        CoordX = 0
        CoordY = 0
        escalaImagen = 0
        Ancho = 0
        Largo = 0
        sensibilidad = 0
        error = 0
        djkstra = 0


    def resultados(self):


        fichero5 = open("Grafos", "rb")
        ruta = pickle.load(fichero5)
        NodosObstaculos = pickle.load(fichero5)
        Matrizimg = pickle.load(fichero5)
        Nodos = pickle.load(fichero5)
        MatrizPesoCorregida = pickle.load(fichero5)
        CoordX = pickle.load(fichero5)
        CoordY = pickle.load(fichero5)
        fichero5.close()

        DatosEscala2 = open("Escala", "rb")
        escalaImagen = pickle.load(DatosEscala2)
        Ancho = pickle.load(DatosEscala2)
        Velocidad = pickle.load(DatosEscala2)
        sensibilidad = pickle.load(DatosEscala2)
        error = pickle.load(DatosEscala2)
        djkstra = pickle.load(DatosEscala2)
        DatosEscala2.close()

        escalaImagen2 = (1 / escalaImagen)
        escalaImagen2 = round(escalaImagen2)
        escalaImagen2 = int(escalaImagen2)

        Distancia = ((djkstra[1] * (1 / escalaImagen)) / 37.795275)

        Segundos = Distancia / Velocidad
        Dias = Segundos // (24 * 60 * 60)
        Segundos = Segundos % (24 * 60 * 60)
        Horas = Segundos // (60 * 60)
        Segundos = Segundos % (60 * 60)
        Minutos = Segundos // 60
        Segundos = Segundos % 60

        ventanaResu = Toplevel()
        ventanaResu.geometry("380x550+450+100")
        ventanaResu.title("Resultados")
        ventanaResu.configure(bg=color2)

        textcaja = Label(ventanaResu, text=" Resultados de la simulación", bg=color1, fg="white")
        textcaja.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

        ventanaTexto = Text(ventanaResu)  # , width=30, height=10)
        ventanaTexto.pack(side=TOP, padx=5, pady=5, ipadx=5, ipady=5, fill=X)

        boton2 = Button(ventanaResu, text='Salir', command=ventanaResu.destroy)
        boton2.pack(side=TOP, padx=5, pady=5, ipadx=5, ipady=5, fill=X)

        ventanaTexto.delete('1.0', END)
        ventanaTexto.insert(INSERT, "URL: " + str(ruta) + " . \n")
        ventanaTexto.insert(INSERT, "Punto Inicial: " + str(NodosObstaculos[0]) + ". \n")
        ventanaTexto.insert(INSERT, "Punto Final: " + str(NodosObstaculos[-1]) + ". \n")
        ventanaTexto.insert(INSERT, "Número de Nodos: " + str(Nodos) + ". \n")
        ventanaTexto.insert(INSERT, "Nodos de la imagen: " + str(NodosObstaculos) + " . \n")
        ventanaTexto.insert(INSERT, "Escala ----> 1 : " + str(escalaImagen2) + " . \n")
        ventanaTexto.insert(INSERT, "La ruta mas corta por Djkstra es: " + str(djkstra) + " . \n")
        if (djkstra[1] == inf):
            ventanaTexto.insert(INSERT, "El punto de partida o de llegada, se encuentra sobre un obstáculo. \n")
        else:
            ventanaTexto.insert(INSERT, "La distancia recorrida en centimetros por el robot es: " + str(
                Distancia) + " centímetros . \n")
            ventanaTexto.insert(INSERT, "La distancia recorrida en metros por el robot es: " + str(
                Distancia / 100) + " mts . \n")
            ventanaTexto.insert(INSERT, "Tiempo de recorrido: Dias:" + str(int(Dias)) + " Horas:" + str(
                int(Horas)) + " Minutos:" + str(int(Minutos)) + " Segundos:" + str(Segundos) + ". \n")


    def guardarArchivo(self):

        fichero5 = open("Grafos", "rb")
        ruta = pickle.load(fichero5)
        NodosObstaculos = pickle.load(fichero5)
        Matrizimg = pickle.load(fichero5)
        Nodos = pickle.load(fichero5)
        MatrizPesoCorregida = pickle.load(fichero5)
        CoordX = pickle.load(fichero5)
        CoordY = pickle.load(fichero5)
        fichero5.close()

        DatosEscala2 = open("Escala", "rb")
        escalaImagen = pickle.load(DatosEscala2)
        Ancho = pickle.load(DatosEscala2)
        Velocidad = pickle.load(DatosEscala2)
        sensibilidad = pickle.load(DatosEscala2)
        error = pickle.load(DatosEscala2)
        djkstra = pickle.load(DatosEscala2)
        DatosEscala2.close()

        escalaImagen2 = (1 / escalaImagen)
        escalaImagen2 = round(escalaImagen2)
        escalaImagen2 = int(escalaImagen2)

        Distancia = ((djkstra[1] * (1 / escalaImagen)) / 37.795275)

        Segundos = Distancia / Velocidad
        Dias = Segundos // (24 * 60 * 60)
        Segundos = Segundos % (24 * 60 * 60)
        Horas = Segundos // (60 * 60)
        Segundos = Segundos % (60 * 60)
        Minutos = Segundos // 60
        Segundos = Segundos % 60

        file = filedialog.asksaveasfilename(title="Guardar como", defaultextension='.txt')

        archivo_texto = open(file, "w")
        archivo_texto.write("URL: " + str(ruta) + " . \n")
        archivo_texto.write("Punto Inicial: " + str(NodosObstaculos[0]) + ". \n")
        archivo_texto.write( "Punto Final: " + str(NodosObstaculos[-1]) + ". \n")
        archivo_texto.write("Número de Nodos: " + str(Nodos) + ". \n")
        archivo_texto.write("Nodos de la imagen: " + str(NodosObstaculos) + " . \n")
        archivo_texto.write("La ruta mas corta por Djkstra es: " + str(djkstra) + " . \n")
        if (djkstra[1] == inf):
            archivo_texto.write( "El punto de partida o de llegada, se encuentra sobre un obstáculo. \n")
            archivo_texto.close()
        else:
            archivo_texto.write("La distancia recorrida en centimetros por el robot es: " + str(
                Distancia) + " centímetros . \n")
            archivo_texto.write("La distancia recorrida en metros por el robot es: " + str(
                Distancia / 100) + " mts . \n")
            archivo_texto.write( "Tiempo de recorrido: Dias:" + str(int(Dias)) + " Horas:" + str(
                int(Horas)) + " Minutos:" + str(int(Minutos)) + " Segundos:" + str(Segundos) + ". \n")
            archivo_texto.close()



        # file = filedialog.asksaveasfilename(title="Guardar como", defaultextension='.png')
        # x = ventana.winfo_rootx() + canvas.winfo_x()
        # y = ventana.winfo_rooty() + canvas.winfo_y()
        # x1 = x + ventana.winfo_width()
        # y1 = y + ventana.winfo_height()
        # ImageGrab.grab().crop((x, y, x1, y1)).save(file)


def acercaDe():
    cajadeTexto = Toplevel()
    cajadeTexto.geometry("380x550+450+100")
    cajadeTexto.title("Resultados")
    cajadeTexto.configure(bg=color2)

    textcaja = Label(cajadeTexto, text=" Simulador Path Planning: Grafos de Visibilidad", bg=color1, fg="white")
    textcaja.pack(padx=5, pady=5, ipadx=5, ipady=5, fill=X)

    ventanaText = Text(cajadeTexto)  # , width=30, height=10)
    ventanaText.pack(side=TOP, padx=5, pady=5, ipadx=5, ipady=5, fill=X)
    ventanaText.configure(font=("Helvetica", 13))

    boton2 = Button(cajadeTexto, text='Cerrar', command=cajadeTexto.destroy)
    boton2.pack(side=TOP, padx=5, pady=5, ipadx=5, ipady=5, fill=X)
    ventanaText.insert(INSERT, "Universidad Distrital Francisco José de Caldas - Facultad Tecnologica \n")
    ventanaText.insert(INSERT, "Autores: \n")
    ventanaText.insert(INSERT, "Jorge A. Duarte A., Ing \n")
    ventanaText.insert(INSERT, "Yeisson F. Triviño R., Ing \n")
    ventanaText.insert(INSERT, "Fredy H. Martínez S., Ph.D \n")
    ventanaText.insert(INSERT, "  \n")
    ventanaText.insert(INSERT, "Grupo de investigación ARMOS - Arquitecturas Modernas para Sistemas de Alimentación\n  \n")
    ventanaText.insert(INSERT, "Versión 1.0 \n \n \n \n")
    ventanaText.insert(INSERT, "Copyright © Todos los derechos reservados \n")


#---------Pocision Inicial de punto de inicio
# Creamos un diccionario que nos permmita guardar las coordenadas y el nombre del objeto
global img, img2
img = tkinter.PhotoImage(file=r"inicio.png")

def puntoInicial():
    global Boton_Inicial, Boton_Final
    Boton_Inicial.config(state= DISABLED)
    Boton_Final.config(state=NORMAL)
    global img, img2
    # PARTE GRÁFICA
    canvas.tag_bind("imgx1", "<ButtonPress-1>", imgPress)
    canvas.tag_bind("imgx1", "<ButtonRelease-1>", imgRelease)
    canvas.tag_bind("imgx1", "<B1-Motion>", imgMotion)
    canvas.create_image(10, 10, anchor=tkinter.CENTER, image=img, tags="imgx1")


posicionInicial = {"x": 0, "y": 0, "imgx1": None}

# Funcion que permite guardar en el diccionario anterior los datos de un objeto sobre el que presionamos con el raton
def imgPress(event):
    global img, img2
    posicionInicial["item"] = "imgx1"
    posicionInicial["x"] = int(canvas.canvasx(event.x))
    posicionInicial["y"] = int(canvas.canvasy(event.y))



# Funcion que permite reiniciar el diccionario cuando se sulta un objeto para poder usarlo de nuevo
def imgRelease(event):
    global img, img2
    posicionInicial["item"] = None
    posicionInicial["x"] = 0
    posicionInicial["y"] = 0


# Funcion que calcula el desplazamiento y usa el metodo move() de Canvas para reposicionar el item.
def imgMotion(event):
    global img, img2
    canvas.delete("arista")
    incremento_x = int(canvas.canvasx(event.x)) - posicionInicial["x"]
    incremento_y = int(canvas.canvasy(event.y)) - posicionInicial["y"]

    canvas.move(posicionInicial["item"], incremento_x, incremento_y)
    posicionInicial["x"] = int(canvas.canvasx(event.x))
    posicionInicial["y"] = int(canvas.canvasy(event.y))
    print("posicionx,posicionyinicial", posicionInicial["x"], posicionInicial["y"])
    canvas.delete("aristaentreelementos")
    canvas.delete("CaminoIdeal")
    canvas.delete("Puntos")
    canvas.delete("line")
    resultoFinal(1)



#---------Pocision Inicial de punto de final
#-----------------------------------------------------------------------------------------------------------------------


# Creamos un diccionario que nos permmita guardar las coordenadas y el nombre del objeto
img2 = tkinter.PhotoImage(file=r"inicio2.png")
#img2 = tkinter.PhotoImage(file=r"C:\Users\usuario\Desktop\PY\MAtriZ\inicio2.png")
def puntoFinal():
    global img, img2
    global Boton_Final, Boton_Rutas
    Boton_Final.config(state=DISABLED)
    Boton_Rutas.config(state=NORMAL)
    # PARTE GRÁFICA
    canvas.tag_bind("img2x2", "<ButtonPress-1>", imgPressFinal)
    canvas.tag_bind("img2x2", "<ButtonRelease-1>", imgReleaseFinal)
    canvas.tag_bind("img2x2", "<B1-Motion>", imgMotionFinal)
    canvas.create_image(10, 10, anchor=tkinter.CENTER, image=img2, tags="img2x2")

posicionFinal  = {"a": 0, "b": 0, "img2x2": None}
# Funcion que permite guardar en el diccionario anterior los datos de un objeto sobre el que presionamos con el raton
def imgPressFinal(event):
    global img, img2
    posicionFinal["item"] = "img2x2"
    posicionFinal["a"] = int(canvas.canvasx(event.x))
    posicionFinal["b"] = int(canvas.canvasy(event.y))




# Funcion que permite reiniciar el diccionario cuando se sulta un objeto para poder usarlo de nuevo
def imgReleaseFinal(event):
    global img, img2
    posicionFinal["item"] = None
    posicionFinal["a"] = 0
    posicionFinal["b"] = 0


# Funcion que calcula el desplazamiento y usa el metodo move() de Canvas para reposicionar el item.
def imgMotionFinal(event):
    global img, img2
    canvas.delete("arista")
    incremento_xFinal = int(canvas.canvasx(event.x)) - posicionFinal["a"]
    incremento_yFinal = int(canvas.canvasy(event.y)) - posicionFinal["b"]

    canvas.move(posicionFinal["item"], incremento_xFinal, incremento_yFinal)
    posicionFinal["a"] = int(canvas.canvasx(event.x))
    posicionFinal["b"] = int(canvas.canvasy(event.y))
    print("posicionx,posicionyfinal",posicionFinal["a"],posicionFinal["b"])
    canvas.delete("aristaentreelementos")
    canvas.delete("CaminoIdeal")
    canvas.delete("Puntos")
    canvas.delete("line")
    resultoFinal(2)


def resultoFinal(decision):    #CooInicial = Coordenada de llegada

    fichero6 = open("Grafos", "rb")
    ruta = pickle.load(fichero6)
    NodosObstaculos = pickle.load(fichero6)
    Matrizimg = pickle.load(fichero6)
    fichero6.close()

    if decision == 1 :
        NodosObstaculos.pop(0)
        NodosObstaculos.insert(0, [posicionInicial["x"], posicionInicial["y"]])
    else:
        NodosObstaculos.pop(-1)
        NodosObstaculos.append([posicionFinal["a"], posicionFinal["b"]])

    fichero_binario = open("Grafos", "wb")
    pickle.dump(ruta, fichero_binario)
    pickle.dump(NodosObstaculos, fichero_binario)
    pickle.dump(Matrizimg, fichero_binario)
    fichero_binario.close()


def buscarRutas():
    global Boton_Rutas, Boton_Dijkstra
    global CoordenadasX, CoordenadasY
    Boton_Rutas.config(state=DISABLED)
    Boton_Dijkstra.config(state=NORMAL)


    fichero7 = open("Grafos", "rb")
    ruta = pickle.load(fichero7)
    NodosObstaculos = pickle.load(fichero7)
    Matrizimg = pickle.load(fichero7)
    fichero7.close()

    DatosEscala = open("Escala", "rb")
    escalaImagen = pickle.load(DatosEscala)
    Ancho = pickle.load(DatosEscala)
    Largo = pickle.load(DatosEscala)
    sensibilidad = pickle.load(DatosEscala)
    error = pickle.load(DatosEscala)
    DatosEscala.close()

    #TOTALES = NodosObstaculos

    CoordX = []
    CoordY = []
    CoordenadasX = []
    CoordenadasY = []

    Totaloordenadas = 0
    Longitud = len(NodosObstaculos)
    Medidor = 0

    CantidadObstaculos = (len(NodosObstaculos))

    CantidadObjetos=[]
    for z in range(int(CantidadObstaculos)):
        CantidadObjetos.append(int((len(NodosObstaculos[z])) / 2))

    xt = []

    for a in range(len(NodosObstaculos)):
        xt = xt.__add__(NodosObstaculos[a])
    for a in range(len(xt)):
        if a % 2 == 0:
            CoordX.append(xt[a])
        else:
            CoordY.append(xt[a])




    for contador in range(Longitud):
        Totaloordenadas = Totaloordenadas + len(NodosObstaculos[contador])

    Nodos = Totaloordenadas / 2

    MatrizPesoMal = np.zeros((int(Nodos), int(Nodos)))
    MatrizPesoCorregida = np.zeros((int(Nodos), int(Nodos)))
    MatrizObstaculos = np.zeros((int(Nodos), int(Nodos)))

    for x in range(len(CoordX)):

        for y in range(len(CoordY)):

            bresenh = list(bresenham(CoordX[x], CoordY[x], CoordX[y], CoordY[y]))

            Sumatoria = 0
            Eval = 0
            for k1 in range(len(bresenh)):
                Eval = (Matrizimg[bresenh[k1][0], bresenh[k1][1]])

                Sumatoria = int(Sumatoria + Eval)

            if (int(Sumatoria) < error):  # existe obstaculo  #-26   #
                MatrizPesoMal[x][y] = (math.sqrt(((CoordX[x] - CoordX[y]) ** 2) + ((CoordY[x] - CoordY[y]) ** 2)))

                Sumatoria = 0
            else:

                MatrizPesoMal[x][y] = (math.sqrt(((CoordX[x] - CoordX[y]) ** 2) + ((CoordY[x] - CoordY[y]) ** 2)))

                canvas.create_line(CoordX[x], CoordY[x], CoordX[y], CoordY[y], tag="aristaentreelementos", fill="gray54",
                                   width=1, dash=(1, 1, 1, 1), smooth=True)
                MatrizObstaculos[x][y] = 1

    VectorContador = []
    # Dibujando lineas de los obstaculos

    CoordenadasX = []  # N0         N1       N2       N3           N4
    CoordenadasY = []

    for yt in range(CantidadObstaculos - 2):  # [[20, 20], [857, 24, 728, 224, 985, 226], [1000, 500]]
        CoordenadasX = []  # N0         N1       N2       N3           N4
        CoordenadasY = []

        for yr in range(CantidadObjetos[yt + 1]):
            CoordenadasX.append(NodosObstaculos[yt + 1][2 * yr])
            CoordenadasY.append(NodosObstaculos[yt + 1][2 * yr + 1])

        rf = len(CoordenadasX)

        for yf in range(rf - 1):
            Medidor = Medidor + 1
            if (yf == 0):
                Medidor2 = Medidor

            VectorContador.append(Medidor)
            MatrizObstaculos[Medidor][Medidor + 1] = 1
            MatrizObstaculos[Medidor + 1][Medidor] = 1
            canvas.create_line(CoordenadasX[yf], CoordenadasY[yf], CoordenadasX[yf + 1], CoordenadasY[yf + 1],
                               tag="aristaentreelementos", fill="red", width=1, dash=(1, 1, 1, 1), smooth=True)

        canvas.create_line(CoordenadasX[0], CoordenadasY[0], CoordenadasX[-1], CoordenadasY[-1],
                           tag="aristaentreelementos", fill="green", width=1, dash=(1, 1, 1, 1), smooth=True)
        Medidor = Medidor + 1
        VectorContador.append(Medidor)
        MatrizObstaculos[Medidor][Medidor2] = 1
        MatrizObstaculos[Medidor2][Medidor] = 1



    for a in range(len(CoordX)):
        for b in range(len(CoordY)):
            if (MatrizObstaculos[a][b] == 1):
                MatrizPesoCorregida[a][b] = MatrizPesoMal[a][b]



    ficherok_binario = open("Grafos", "wb")
    pickle.dump(ruta, ficherok_binario)
    pickle.dump(NodosObstaculos, ficherok_binario)
    pickle.dump(Matrizimg, ficherok_binario)
    pickle.dump(Nodos, ficherok_binario)
    pickle.dump(MatrizPesoCorregida, ficherok_binario)
    pickle.dump(CoordX, ficherok_binario)
    pickle.dump(CoordY, ficherok_binario)
    ficherok_binario.close()




def mostrarOriginal():

    fichero0 = open("Grafos", "rb")
    ruta = pickle.load(fichero0)
    fichero0.close()
    global photo
    photo = PhotoImage(file=ruta)
    canvas.create_image(0, 0, image=photo, anchor=NW)

def nuevaConfiguracion():
    fuchero = open("Grafos", "rb")
    ruta = pickle.load(fuchero)
    fuchero.close()

    fichero_bin = open("Grafos", "wb")
    pickle.dump(ruta, fichero_bin)
    fichero_bin.close()

    Opciones().activarEscala()




#_______________________________________________________________________________________________________________________
class Opciones:
    def __init__(self):
        self.Objetos =[]


    def abrirArchivo(self):

        try:
            archivo = filedialog.askopenfile(title="abrir",
                                             filetypes=(("Archivos PNG", "*.png"), ("Archivo JPG ", "*.jpg"),))
            ruta = archivo.name

            fichero_b4inario = open("Grafos", "wb")
            pickle.dump(ruta, fichero_b4inario)
            fichero_b4inario.close()

        except:
            print("No ha sido posible cargar la imagen")

        global photo,Boton_Borde
        photo = ImageTk.PhotoImage(Image.open(ruta))
        canvas.create_image(0, 0, image=photo, anchor=NW)

        Boton_Borde.config(state=NORMAL)
        cajaEscala()

    def activarEscala(self):

        global Boton_Borde
        Boton_Borde.config(state=DISABLED)

        Opciones().abrirEnlace(1)






    def eliminar(self):

        canvas.delete("all")

    def quit(self):
        self.ventana.destroy()

    def abrirEnlace(self, proceso):


        CoordenadasX=[]
        CoordenadasY=[]

        canvas.delete("aristaentreelementos")
        canvas.delete("CaminoIdeal")
        canvas.delete("Puntos")
        canvas.delete("ovalos")
        canvas.delete("img2x2")
        canvas.delete("imgx1")
        canvas.delete("line")


        global Boton_Eliminar,Boton_Guardar, Boton_Abrir
        Boton_Guardar.config(state=DISABLED)
        Boton_Eliminar.config(state=NORMAL)
        Boton_Abrir.config(state=NORMAL)
        self.proceso = proceso



        fochero = open("Grafos", "rb")
        ruta = pickle.load(fochero)
        fochero.close()

        DatosEscala3 = open("Escala", "rb")
        escalaImagen = pickle.load(DatosEscala3)
        Ancho = pickle.load(DatosEscala3)  # En centimetros
        Largo = pickle.load(DatosEscala3)
        sensibilidad = pickle.load(DatosEscala3)
        error = pickle.load(DatosEscala3)
        DatosEscala3.close()



        incremento = round((Ancho * escalaImagen * 37.795275))

        if (proceso == 1):
            self.ruta =ruta

        elif (proceso == 2):
            self.ruta = 'Modificada.png'

        img = cv2.imread(self.ruta, cv2.IMREAD_GRAYSCALE)

        (thresh, im_bw) = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        canny = cv2.Canny(im_bw, 10, 150)
        canny = cv2.dilate(canny, None, iterations=1)
        canny = cv2.erode(canny, None, iterations=1)
        height, width = img.shape  # Se obteniene la altura y el ancho de la imagen

        # contours, _ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # OpenCV 4
        contours, _ = cv2.findContours(canny, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  # OpenCV 4


        if proceso ==2:
            Matrizimg = np.zeros((width, height))  # Se crea una matriz de ceros igual a los pixeles de ancho y alto
            for w in range(width):
                for y in range(height):

                    if (img[y, w] == 255):  # Blanco
                        Matrizimg[w, y] = 0  # Se discrimina la matriz por color

                    else:
                        Matrizimg[w, y] = -1


        ciclo = 0
        for cnt in contours:
            ciclo = ciclo + 1
            approx = cv2.approxPolyDP(cnt,   sensibilidad * cv2.arcLength(cnt, True), True)  # 0.005    # 0.0009 #0.0005
            cv2.drawContours(im_bw, [approx], 0, (0, 0, 0), incremento,cv2.LINE_AA)

            n = approx.ravel()
            Coord = tuple(n)
            Coord2 = list(n)
            NumeroVertices = int((len(Coord) / 2))


            if ciclo % 2 == 0:
                self.Objetos.append(Coord2)

            if (self.proceso == 2):

                if ciclo % 2 == 0:

                    #canvas.create_polygon(Coord, outline="snow", fill='gray4', joinstyle=ROUND, activefill='gray12',
                    #                      width=2, smooth=0)  # dash 4
                    canvas.create_polygon(Coord, outline="snow",width=2,fill='', tag = "line")  # dash 4


                Diametro = 8
                for LK in range(NumeroVertices):
                    if ciclo % 2 == 0:
                        canvas.create_oval(Coord[2 * LK] - Diametro, Coord[2 * LK + 1] - Diametro, Coord[2 * LK] + Diametro,
                                           Coord[2 * LK + 1] + Diametro, fill="snow", tag="ovalos")

                        LK = LK + 2
                LK = 0


        if (self.proceso) == 1 :
            cv2.imwrite('Modificada.png', im_bw)
            Opciones().espesorObstaculo()


        else:
            NodosObstaculos = self.Objetos


            NodosObstaculos.insert(0, [0, 0])
            NodosObstaculos.append([0, 0])


            #fichero_binario = open("Grafos", "wb")
            #pickle.dump(ruta, fichero_binario)
            #pickle.dump(NodosObstaculos, fichero_binario)
            #pickle.dump(Matrizimg, fichero_binario)
            #pickle.dump(Nodos, fichero_binario)



            fichero_binario2= open("Grafos", "wb")
            pickle.dump(ruta, fichero_binario2)
            pickle.dump(NodosObstaculos, fichero_binario2)
            pickle.dump(Matrizimg, fichero_binario2)
            fichero_binario2.close()

        global Boton_Inicial
        Boton_Inicial.config(state = NORMAL)





    def espesorObstaculo(self):
        self.proceso = 2

        Opciones().abrirEnlace(2)

class Vertice:
    def __init__(self, i):
        self.id = i
        self.vecinos = []
        self.visitado = False
        self.padre = None
        self.distancia = float('inf')

    def agregarVecino(self, v, p):
        if (v not in self.vecinos):
            self.vecinos.append([v, p])

class Grafica:
    def __init__(self):
        self.vertices = {}

    def agregarVertice(self, id):
        if (id not in self.vertices):
            self.vertices[id] = Vertice(id)

    def agregarArista(self, a, b, p):
        if (a in self.vertices and b in self.vertices):
            self.vertices[a].agregarVecino(b, p)
            self.vertices[b].agregarVecino(a, p)

    def imprimirGrafica(self):
        for v in self.vertices:
            print("la distancia del vertice " + str(v) + " es " + str(
                self.vertices[v].distancia) + "llegando desde" + str(self.vertices[v].padre))

    def camino(self, a, b):
        camino = []
        actual = b
        while actual != None:
            camino.insert(0, actual)
            actual = self.vertices[actual].padre
        return [camino, self.vertices[b].distancia]

    def minimo(self, lista):
        if len(lista) > 0:
            m = self.vertices[lista[0]].distancia
            v = lista[0]

            for e in lista:
                if (m > self.vertices[e].distancia):
                    m = self.vertices[e].distancia
                    v = e
            return v

    def dijkstra(self, a):
        if (a in self.vertices):
            self.vertices[a].distancia = 0
            actual = a
            noVisitados = []

            for v in self.vertices:
                if (v != a):
                    self.vertices[v].distancia = float('inf')
                self.vertices[v].padre = None
                noVisitados.append(v)

            while len(noVisitados) > 0:
                for vecino in self.vertices[actual].vecinos:
                    if self.vertices[vecino[0]].visitado == False:
                        if self.vertices[actual].distancia + vecino[1] < self.vertices[vecino[0]].distancia:
                            self.vertices[vecino[0]].distancia = self.vertices[actual].distancia + vecino[1]
                            self.vertices[vecino[0]].padre = actual

                self.vertices[actual].visitado = True
                noVisitados.remove(actual)

                actual = self.minimo(noVisitados)


        else:
            return False






def rutaMasRapida():
    global Boton_Rutas,Boton_Guardar, Boton_Resultado
    Boton_Guardar.config(state=NORMAL)
    Boton_Resultado.config(state=NORMAL)
    Boton_Rutas.config(state=NORMAL)
    fiichero = open("Grafos", "rb")
    ruta = pickle.load(fiichero)
    NodosObstaculos = pickle.load(fiichero)
    Matrizimg = pickle.load(fiichero)
    Nodos = pickle.load(fiichero)
    MatrizPesoCorregida = pickle.load(fiichero)
    CoordX = pickle.load(fiichero)
    CoordY = pickle.load(fiichero)
    fiichero.close()


    g = Grafica()
    Diametro = 7
    for a in range(int(Nodos)):
        g.agregarVertice(a)

    for a in range(int(Nodos)):
        for b in range(int(Nodos)):
            if (a != b):
                if (a < b):
                    if (MatrizPesoCorregida[a][b] != 0):

                        g.agregarArista(a, b, MatrizPesoCorregida[a][b])

    #print("La ruta mas rapida por djkstra")

    DatosEscala2 = open("Escala", "rb")
    escalaImagen = pickle.load(DatosEscala2)
    Ancho = pickle.load(DatosEscala2)
    Largo = pickle.load(DatosEscala2)
    sensibilidad = pickle.load(DatosEscala2)
    error = pickle.load(DatosEscala2)
    DatosEscala2.close()




    g.dijkstra(0)

    Dato=(g.camino(0, (int(Nodos)) - 1))
    print("Dato", Dato)



    fichero_Escala3 = open("Escala", "wb")
    pickle.dump(escalaImagen, fichero_Escala3)
    pickle.dump(Ancho, fichero_Escala3)
    pickle.dump(Largo, fichero_Escala3)
    pickle.dump(sensibilidad, fichero_Escala3)
    pickle.dump(error, fichero_Escala3)
    pickle.dump(Dato, fichero_Escala3)
    fichero_Escala3.close()




    # ----------------------------------DJ----------------------------------------------


    Verti = g.camino(0,(int(Nodos))-1)
    print(Verti[0])
    print(len(Verti[0]))
    for a in range(len(Verti[0])):
        canvas.create_oval(CoordX[Verti[0][a]] - Diametro, CoordY[Verti[0][a]] - Diametro,
                    CoordX[Verti[0][a]] + Diametro, CoordY[Verti[0][a]] + Diametro, fill='green2',
                        activefill="green4",tag="Puntos")

    for a in range((len(Verti[0])) - 1):
        canvas.create_line(CoordX[Verti[0][a]], CoordY[Verti[0][a]], CoordX[Verti[0][a + 1]],
                            CoordY[Verti[0][a + 1]],
                            tag="CaminoIdeal", fill="green2", width=2)


    #





    #---------------------------------------------------INICIO------------------------------------------------------------

Abrir = PhotoImage(file = r"abrir.png")
Boton_Abrir = Button(my_frame1, text="Abrir", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Abrir, compound= "top", command = Opciones().abrirArchivo)
Boton_Abrir.grid(row =0, column =0)


Guardar = PhotoImage(file = r"guardar.png")
Boton_Guardar = Button(my_frame1, text="Guardar", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Guardar,compound= "top", command=Salida().guardarArchivo, state= DISABLED)
Boton_Guardar.grid(row =0, column =1)


Eliminar = PhotoImage(file = r"eliminar.png")
Boton_Eliminar = Button(my_frame1, text="Eliminar", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Eliminar,compound= "top", command=Opciones().eliminar, state= DISABLED)
Boton_Eliminar.grid(row =0, column =2)


Salir = PhotoImage(file = r"salir.png")
Boton_Salir = Button(my_frame1, text="Salir", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Salir,compound= "top", command=quit)
Boton_Salir.grid(row =0, column =3)
#---------------------------------------------------ANALIZAR------------------------------------------------------------
Borde = PhotoImage(file = r"borde.png")
Boton_Borde = Button(my_frame2, text="Borde", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Borde,compound= "top", command =nuevaConfiguracion, state= DISABLED)
Boton_Borde.grid(row =0, column =0)

Inicial = PhotoImage(file = r"inicial.png")
Boton_Inicial = Button(my_frame2, text="Inicio", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Inicial,compound= "top", command = puntoInicial, state= DISABLED)
Boton_Inicial.grid(row =0, column =1)


Final = PhotoImage(file = r"final.png")
Boton_Final = Button(my_frame2, text="Destino", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Final,compound= "top", command =puntoFinal, state= DISABLED)
Boton_Final.grid(row =0, column =2)


Rutas = PhotoImage(file = r"nodo.png")
Boton_Rutas = Button(my_frame2, text="Buscar rutas", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Rutas,compound= "top", command = buscarRutas, state= DISABLED)
Boton_Rutas.grid(row =0, column =3)


Dijkstra = PhotoImage(file = r"cohete.png")
Boton_Dijkstra = Button(my_frame2, text="Ruta mas corta", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Dijkstra,compound= "top", command= rutaMasRapida, state= DISABLED)
Boton_Dijkstra.grid(row =0, column =4)
#---------------------------------------------------CONFIGURAR----------------------------------------------------------


Escala1 = PhotoImage(file = r"escala1.png")
Boton_Escala = Button(my_frame3, text="Escala", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Escala1,compound= "top",command=cajaEscala)
Boton_Escala.grid(row =0, column =0)

Resultado = PhotoImage(file = r"resultad.png")
Boton_Resultado = Button(my_frame3, text="Resultados", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Resultado,compound= "top",command=Salida().resultados, state=DISABLED)
Boton_Resultado.grid(row =0, column =1)

#---------------------------------------------------AYUDA---------------------------------------------------------------
Acercade = PhotoImage(file = r"acercade.png")
Boton_Acercade = Button(my_frame4, text="Acerca de...", padx = 25, pady = 25, background=color1,activebackground=color2,fg=letra,image=Acercade,compound= "top",command=acercaDe)
Boton_Acercade.grid(row =0, column =0)


ventana.mainloop()


